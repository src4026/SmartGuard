# SmartGuard
A HexChat addon that aids moderation by reading messages received real-time in channels and narrowing them down into suspicious messages that may contain potential swears. With this tool you don't have to read every single message in the chat you're moderating; this addon narrows everything down to a _way_ smaller collection.

## Ability
Most cases of swear filter bypass are caught.
- Blending with other words: `hellofvck3you`
- Repetition or distortion: `sSsHhh54%*IITtt!`
- Multiline:
    ```
    <player> f
    <player> u
    <player> you
    ```
- Usage of ignorable characters: `s_h_! t`
- Swear word variants: `phuq`

## Installation
- Navigate to your HexChat addons folder and run the following command:
    ```
    git clone https://gitlab.com/OrangeBlob/SmartGuard.git
    ```
- Next, open the cloned folder and open `main.lua` with an editor. Edit the value `script_dir` to specify the directory of your script. For example:
    ```lua
    local script_dir = "C:/Users/John/AppData/Roaming/HexChat/addons/SmartGuard"
    ```
- To load the addon, open HexChat, and under the `HexChat` dropdown menu, click `Load Plugin or Script`. Open the "SmartGuard" folder, select `main.lua`, and click `OK`.
- **Important:** By default, the addon uses your IRC account to send all flagged messages to the ##smartguard channel on IRC. You can edit the channel the flagged messages are sent to by editing the `log_channel` value below `script_dir`.

## Auto-load Addon on Startup
- To load the addon automatically everytime you open HexChat, first, open the `HexChat` dropdown menu and click `Network List`.
- Click `Edit` beside your choosen network to open up the network settings, and click the `Connect commands` tab.
- Click `Add` to add the following connect command:
    ```
    load SmartGuard/main.lua
    ```
- Click `Close` and you're good to go.
